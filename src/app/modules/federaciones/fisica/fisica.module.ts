import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FisicaRoutingModule } from './fisica-routing.module';
import { CatalogoFederacionesFisicasComponent } from './catalogo-federaciones-fisicas/catalogo-federaciones-fisicas.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [CatalogoFederacionesFisicasComponent],
  imports: [
    CommonModule,
    FisicaRoutingModule,
    NgxDatatableModule
  ]
})
export class FisicaModule { }
