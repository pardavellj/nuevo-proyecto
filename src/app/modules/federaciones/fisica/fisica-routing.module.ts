import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoFederacionesFisicasComponent } from './catalogo-federaciones-fisicas/catalogo-federaciones-fisicas.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'catalogo',
        component: CatalogoFederacionesFisicasComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FisicaRoutingModule { }
