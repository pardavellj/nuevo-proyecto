import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoFederacionesFisicasComponent } from './catalogo-federaciones-fisicas.component';

describe('CatalogoFederacionesFisicasComponent', () => {
  let component: CatalogoFederacionesFisicasComponent;
  let fixture: ComponentFixture<CatalogoFederacionesFisicasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoFederacionesFisicasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoFederacionesFisicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
