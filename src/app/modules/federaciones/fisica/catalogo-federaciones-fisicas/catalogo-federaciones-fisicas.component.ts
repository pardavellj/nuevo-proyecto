import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-catalogo-federaciones-fisicas',
  templateUrl: './catalogo-federaciones-fisicas.component.html',
  styleUrls: ['./catalogo-federaciones-fisicas.component.scss']
})
export class CatalogoFederacionesFisicasComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

  rows = [];
  temp = [];

  isCollapse = true;

  dataTable = [{
    idPersona: 1,
    paterno: 'Mondragon',
    materno: 'Castillo',
    nombre: 'Arturo',
    curp: 'MOCA970726HDFLTNF4',
    fechaNacimiento: '1997/07/26',
    fechaRegistro: '2021/07/26',
    fechaModificacion: '2021/09/26',
    activo: 1,
    idEscolaridad: 1,
    idEstusEscolaridad: 1,
    calle: 'Sauce',
    numExt: '15',
    NumInt: '0',
    idGenero: 1,
    extranjero: '',
    tipoIdentificacion: 'INE',
    numeroIdentificacion: '00192837645',
    catUbicaciongeograficaId: 2
  }];

  constructor() {
    console.log('constructor');
    this.rows = this.dataTable;
    this.temp = [...this.dataTable];
  }

  ngOnInit() {
    // this.rows = this.dataTable;
    // this.temp = [...this.dataTable];
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.asamblea.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  goToEdit(value: object) {
    // this.router.navigate(['albums/edit/' + value['id']]);
    console.log('value');
    console.log(value);
  }

  edit() {
    console.log('button');
  }

}
