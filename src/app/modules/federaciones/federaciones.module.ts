import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FederacionesRoutingModule } from './federaciones-routing.module';
import { FisicaModule } from './fisica/fisica.module';
import { MoralModule } from './moral/moral.module';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FederacionesRoutingModule,
    FisicaModule,
    MoralModule,
    NgxDatatableModule
  ]
})
export class FederacionesModule { }
