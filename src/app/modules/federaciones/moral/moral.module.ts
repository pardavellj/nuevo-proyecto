import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoralRoutingModule } from './moral-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MoralRoutingModule
  ]
})
export class MoralModule { }
