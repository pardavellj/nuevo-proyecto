import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'fisica',
        // canActivate: [LoginGuard],
        loadChildren: () => import('./../../modules/federaciones/fisica/fisica.module').then(m => m.FisicaModule)
      },
      {
        path: 'moral',
        // canActivate: [LoginGuard],
        loadChildren: () => import('./../../modules/federaciones/moral/moral.module').then(m => m.MoralModule)
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FederacionesRoutingModule { }
