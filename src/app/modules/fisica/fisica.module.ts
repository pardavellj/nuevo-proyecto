import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FisicaRoutingModule } from './fisica-routing.module';
import { CatalogoPersonaFisicaComponent } from './catalogo-persona-fisica/catalogo-persona-fisica.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PersonasFisicasComponent } from './personas-fisicas/personas-fisicas.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    CatalogoPersonaFisicaComponent,
    PersonasFisicasComponent
  ],
  imports: [
    CommonModule,
    FisicaRoutingModule,
    NgxDatatableModule,
    SharedModule
  ],

})
export class FisicaModule { }
