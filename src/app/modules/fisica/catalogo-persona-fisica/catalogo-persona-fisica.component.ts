import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-catalogo-persona-fisica',
  templateUrl: './catalogo-persona-fisica.component.html',
  styleUrls: ['./catalogo-persona-fisica.component.scss']
})
export class CatalogoPersonaFisicaComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

  data = [{ 'personaFisica': 'Arturo Mondragon Castillo'}];

  rows = [];
  temp = [];

  constructor(private router: Router) {
    this.rows = this.data;
    this.temp = [...this.data];
   }

  ngOnInit(): void {
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter((d) => {
      console.log(d);
      return d.personaFisica.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  goToVerMas(value: object) {
    this.router.navigate(['/persona-fisica/more/' + value['personaFisica']]);
    console.log(value);
  }

}
