import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoPersonaFisicaComponent } from './catalogo-persona-fisica.component';

describe('CatalogoPersonaFisicaComponent', () => {
  let component: CatalogoPersonaFisicaComponent;
  let fixture: ComponentFixture<CatalogoPersonaFisicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoPersonaFisicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoPersonaFisicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
