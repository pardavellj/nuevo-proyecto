import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoPersonaFisicaComponent } from './catalogo-persona-fisica/catalogo-persona-fisica.component';
import { PersonasFisicasComponent } from './personas-fisicas/personas-fisicas.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'catalogo',
        component: CatalogoPersonaFisicaComponent
      },
      /* {
        path: 'more/:pf',
        component: PersonasFisicasComponent
      } */
      {
        path: '',
        component: PersonasFisicasComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FisicaRoutingModule { }
