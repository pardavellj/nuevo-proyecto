import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DireccionesRoutingModule } from './direcciones-routing.module';
import { BadmintonComponent } from './badminton/badminton.component';
// import { GeneralTableComponent } from 'src/app/shared/components/general-table/general-table.component';




@NgModule({
  declarations: [
    // GeneralTableComponent,
    BadmintonComponent
  ],
  exports: [
    DireccionesRoutingModule
  ],
  imports: [
    CommonModule,
    DireccionesRoutingModule,
    NgxDatatableModule,
  ]
})
export class DireccionesModule { }
