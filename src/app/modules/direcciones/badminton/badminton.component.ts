import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-badminton',
  templateUrl: './badminton.component.html',
  styleUrls: ['./badminton.component.scss']
})
export class BadmintonComponent implements OnInit {

  permisos = { campoUno: true, campoDos: false, campoTres: true, campoCuatro: false };
  datos: object; // = {asamablea: 'datoAsamblea', secretario: 'datoSec', vocal: 'datoVocal', vocalDeportista: 'datoVocDep'};

  constructor() {
    // this.datos.asamblea = 'datoAsamblea';
    // this.datos.secretario = 'datoSec';
    // this.datos.vocal = 'datoVocal';
    this.datos = [
      { asamblea: 'datoAsamblea', secretario: 'datoSec', vocal: 'datoVocal', vocalDeportista: 'datoVocDep' },
      { asamblea: 'datoAsamblea2', secretario: 'datoSec2', vocal: 'datoVocal2', vocalDeportista: 'datoVocDep2' },
      { asamblea: 'datoAsamblea3', secretario: 'datoSec3', vocal: 'datoVocal3', vocalDeportista: 'datoVocDep3' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea5', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' }
    ];
  }

  ngOnInit(): void {
  }

}
