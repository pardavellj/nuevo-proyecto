import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasMoralesComponent } from './personas-morales.component';

describe('PersonasMoralesComponent', () => {
  let component: PersonasMoralesComponent;
  let fixture: ComponentFixture<PersonasMoralesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasMoralesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasMoralesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
