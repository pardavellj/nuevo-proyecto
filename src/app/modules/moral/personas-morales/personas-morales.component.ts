import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-personas-morales',
  templateUrl: './personas-morales.component.html',
  styleUrls: ['./personas-morales.component.scss']
})
export class PersonasMoralesComponent implements OnInit {

  permisos = { campoUno: true, campoDos: false, campoTres: true, campoCuatro: false };
  datos: object;
  pf: string;

  constructor(private route: ActivatedRoute, private location: Location) {
    this.datos = [
      { asamblea: 'datoAsamblea', secretario: 'datoSec', vocal: 'datoVocal', vocalDeportista: 'datoVocDep' },
      { asamblea: 'datoAsamblea2', secretario: 'datoSec2', vocal: 'datoVocal2', vocalDeportista: 'datoVocDep2' },
      { asamblea: 'datoAsamblea3', secretario: 'datoSec3', vocal: 'datoVocal3', vocalDeportista: 'datoVocDep3' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea5', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' }
    ];
   }

   

  ngOnInit(): void {
    this.pf = this.route.snapshot.params.pf;
    console.log(this.pf);
  }

  goBack() {
    this.location.back();
  }


  

}

