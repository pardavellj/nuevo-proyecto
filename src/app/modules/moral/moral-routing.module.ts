import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogoPersonaMoralComponent } from './catalogo-persona-moral/catalogo-persona-moral.component';
import { PersonasMoralesCreateComponent } from './personas-morales-create/personas-morales-create.component';
import { PersonasMoralesComponent } from './personas-morales/personas-morales.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'catalogo',
        component: CatalogoPersonaMoralComponent
      },
      {
        path: 'more/:pf',
        component: PersonasMoralesComponent
      },
      {
        path: 'create',
        component: PersonasMoralesCreateComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoralRoutingModule { }
