import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoralRoutingModule } from './moral-routing.module';
import { CatalogoPersonaMoralComponent } from './catalogo-persona-moral/catalogo-persona-moral.component';
import { PersonasMoralesComponent } from './personas-morales/personas-morales.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from 'src/app/shared/shared.module';
import { PersonasMoralesCreateComponent } from './personas-morales-create/personas-morales-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CatalogoPersonaMoralComponent,
    PersonasMoralesComponent,
    PersonasMoralesCreateComponent
  ],
  imports: [
    CommonModule,
    MoralRoutingModule,
    NgxDatatableModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MoralModule { }
