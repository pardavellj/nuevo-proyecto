import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoPersonaMoralComponent } from './catalogo-persona-moral.component';

describe('CatalogoPersonaMoralComponent', () => {
  let component: CatalogoPersonaMoralComponent;
  let fixture: ComponentFixture<CatalogoPersonaMoralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoPersonaMoralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoPersonaMoralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
