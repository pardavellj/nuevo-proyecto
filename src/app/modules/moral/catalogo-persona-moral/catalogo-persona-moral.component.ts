import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-catalogo-persona-moral',
  templateUrl: './catalogo-persona-moral.component.html',
  styleUrls: ['./catalogo-persona-moral.component.scss']
})
export class CatalogoPersonaMoralComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

  data = [{ 'personaMoral': 'badminton'}];

  rows = [];
  temp = [];

  constructor(private router: Router) {
    this.rows = this.data;
    this.temp = [...this.data];
   }

  ngOnInit(): void {
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter((d) => {
      console.log(d);
      return d.personaFisica.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  goToVerMas(value: object) {
    this.router.navigate(['/persona-moral/more/' + value['personaMoral']]);
    console.log(value);
  }

}
