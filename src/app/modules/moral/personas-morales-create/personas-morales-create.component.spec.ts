import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasMoralesCreateComponent } from './personas-morales-create.component';

describe('PersonasMoralesCreateComponent', () => {
  let component: PersonasMoralesCreateComponent;
  let fixture: ComponentFixture<PersonasMoralesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasMoralesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasMoralesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
