import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-personas-morales-create',
  templateUrl: './personas-morales-create.component.html',
  styleUrls: ['./personas-morales-create.component.scss']
})
export class PersonasMoralesCreateComponent implements OnInit {

  personaMoralForm: FormGroup;

  constructor( public router: Router) {
    this.personaMoralForm = new FormGroup({
      'descripcion': new FormControl(null, [Validators.required]),
      'titulo': new FormControl(null, [Validators.required]),
      'cargo': new FormControl(null, [Validators.required]),
      'calle': new FormControl(null, [Validators.required]),
      'numExt': new FormControl(null, [Validators.required]),
      'numInt': new FormControl(null, [Validators.required]),
      'estado': new FormControl(null, [Validators.required]),
      'municipio': new FormControl(null, [Validators.required]),
      'colonia': new FormControl(null, [Validators.required]),
      'cp': new FormControl(null, [Validators.required]),
      'rfc': new FormControl(null, [Validators.required]),
      '-telefonos': new FormControl(null, [Validators.required]),
      '-email': new FormControl(null, [Validators.required]),
      'contacto': new FormControl(null, [Validators.required]),
      '-titularNombres': new FormControl(null, [Validators.required]),
      '-titularApePat': new FormControl(null, [Validators.required]),
      '-titularApeMat': new FormControl(null, [Validators.required]),
      '-fechaNac': new FormControl(null, [Validators.required]),
      'inclusion': new FormControl(null, [Validators.required]),
      'talla': new FormControl(null, [Validators.required]),
      'catTipoOrganismosId': new FormControl(null, [Validators.required]),
      'catTipoOrganismosAlfa': new FormControl(null, [Validators.required]),
      'catUbicaciongeograficaId': new FormControl(null, [Validators.required]),
      'tblEstatusOrganismoId': new FormControl(null, [Validators.required]),
      'tblEstatusOrganismoIdAlfa': new FormControl(null, [Validators.required]),
      // textArea: new FormControl(null, [Validators.required]),
    });
   }

  ngOnInit(): void {

  }

  Submit() {
   
  }

}
