export interface GeneralTable {
    asamblea: string;
    vicepresidente: string;
    secretario: string;
    tesorero: string;
    comisario: string;
    vocal: string;
    vocalDeportista: string;
    vencimiento: string;
}
