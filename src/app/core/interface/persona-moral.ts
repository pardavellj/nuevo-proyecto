export interface PersonaMoral {
    descripcion: string;
    cat_tipo_organismos_id: number;
    cat_tipo_organismos_alfa: string;
    titulo: string;
    cargo: string;
    calle: string;
    num_ext: string;
    num_int: string;
    estado: string;
    municipio: string;
    colonia: string;
    cat_ubicaciongeografica_id: number;
    cp: string;
    rfc: string;
    telefonos: string;
    email: string;
    contacto: string;
    foto: string;
    titular_nombres: string;
    titular_ape_pat: string;
    titular_ape_mat: string;
    fecha_nac: string;
    inclusion: string;
    tbl_estatus_organismo_id: number;
    tbl_estatus_organismo_id_alfa: string;
    Talla: string;
}
