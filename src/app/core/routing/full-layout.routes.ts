import { Routes } from '@angular/router';
import { LoginGuard } from '../guards/login.guard';

// Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: 'persona-fisica',
    canActivate: [LoginGuard],
    loadChildren: () => import('./../../modules/fisica/fisica.module').then(m => m.FisicaModule)
  },
  {
    path: 'persona-moral',
    canActivate: [LoginGuard],
    loadChildren: () => import('./../../modules/moral/moral.module').then(m => m.MoralModule)
  },
  {
    path: 'nueva',
    canActivate: [LoginGuard],
    loadChildren: () => import('./../../modules/moral/moral.module').then(m => m.MoralModule)
  }
];
