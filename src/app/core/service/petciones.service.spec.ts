import { TestBed } from '@angular/core/testing';
import { PetcionesService } from './petciones.service';

describe('PetcionesService', () => {
  let service: PetcionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PetcionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
