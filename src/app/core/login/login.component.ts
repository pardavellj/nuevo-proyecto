import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor( public router: Router, private loginService: LoginService) {
    this.loginForm = new FormGroup({
      'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)])
      // textArea: new FormControl(null, [Validators.required]),
      // radioOption: new FormControl('Option one is this')
    });
   }

  ngOnInit(): void {

  }

  loginSubmit() {
    // console.log(this.loginForm.value['inputEmail']);
    // console.log(this.loginForm.value['password']);
    // this.router.navigate(['home']);
    // this.loginService.validateUser().subscribe(data => {
      if (this.loginService.validateUser()) {
        this.router.navigate(['home']);
      }
    // });
  }

}
