import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { BadmintonComponent } from './modules/direcciones/badminton/badminton.component';
import { LayoutComponent } from './shared/layouts/layout/layout.component';
import { LoginComponent } from './core/login/login.component';
import { LoginGuard } from './core/guards/login.guard';
import { Full_ROUTES } from './core/routing/full-layout.routes';


// import { LayoutComponent } from './layouts/full/full-layout.component';
// import { ContentLayoutComponent } from './layouts/content/content-layout.component';


// import { CONTENT_ROUTES } from './shared/routes/content-layout.routes';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
    // component: LoginComponent
  },
  {
    path: 'login',
    // redirectTo: 'dashboard/m/v1',
    // pathMatch: 'full',
    component: LoginComponent
  },
  {
    path: 'home',
    canActivate: [LoginGuard],
    component: LayoutComponent
  },
  {
    path: 'badminton',
    canActivate: [LoginGuard],
    component: BadmintonComponent
  },
  { path: '', component: LayoutComponent, data: { title: 'full Views' }, children: Full_ROUTES },
  // { path: '', component: ContentLayoutComponent, data: { title: 'content Views' }, children: CONTENT_ROUTES },
  // { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

