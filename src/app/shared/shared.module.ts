import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

// import { FooterComponent } from "./footer/footer.component";
// import { NavbarComponent } from "./navbar/navbar.component";
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarMatComponent } from './sidebar-mat/sidebar-mat.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { ContactoPersonaFisicaComponent } from './components/contacto-persona-fisica/contacto-persona-fisica.component';
import { GeneralTableComponent } from './components/general-table/general-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PersonaMoralModalComponent } from './components/modals/persona-moral-modal/persona-moral-modal.component';
// import { ColorSwitcherComponent } from './color-switcher/color-switcher.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PersonaFisicaModalComponent } from './components/modals/persona-fisica-modal/persona-fisica-modal.component';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
    exports: [
        CommonModule,
        // FooterComponent,
        // NavbarComponent,
        SidebarComponent,
        // ColorSwitcherComponent,
        NgbModule,
        SidebarMatComponent,
        MatIconModule,
        GeneralTableComponent,
        ContactoPersonaFisicaComponent,
        MatDialogModule,
        MatButtonModule
    ],
    imports: [
        RouterModule,
        CommonModule,
        NgbModule,
        PerfectScrollbarModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatIconModule,
        MatMenuModule,
        NgxDatatableModule,
        MatDialogModule,
        MatButtonModule
    ],
    declarations: [
        // FooterComponent,
        // NavbarComponent,
        SidebarComponent,
        SidebarMatComponent,
        ContactoPersonaFisicaComponent,
        GeneralTableComponent,
        PersonaMoralModalComponent,
        PersonaFisicaModalComponent
        // ColorSwitcherComponent
    ],
    entryComponents: [
        PersonaMoralModalComponent,
        PersonaFisicaModalComponent
    ],
    providers: [],
})
export class SharedModule { }
