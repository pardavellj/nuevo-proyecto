import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  texto: '';
}


@Component({
  selector: 'app-persona-fisica-modal',
  templateUrl: './persona-fisica-modal.component.html',
  styleUrls: ['./persona-fisica-modal.component.scss']
})
export class PersonaFisicaModalComponent implements OnInit {


  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit(): void {
  }

}
