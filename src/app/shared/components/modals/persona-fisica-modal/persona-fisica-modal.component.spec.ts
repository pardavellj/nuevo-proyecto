import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaFisicaModalComponent } from './persona-fisica-modal.component';

describe('PersonaFisicaModalComponent', () => {
  let component: PersonaFisicaModalComponent;
  let fixture: ComponentFixture<PersonaFisicaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaFisicaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaFisicaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
