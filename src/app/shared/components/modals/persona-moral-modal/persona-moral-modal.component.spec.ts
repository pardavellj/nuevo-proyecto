import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaMoralModalComponent } from './persona-moral-modal.component';

describe('PersonaMoralModalComponent', () => {
  let component: PersonaMoralModalComponent;
  let fixture: ComponentFixture<PersonaMoralModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaMoralModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaMoralModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
