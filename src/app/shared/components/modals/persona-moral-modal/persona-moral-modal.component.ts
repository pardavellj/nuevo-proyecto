import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  texto: '';
}

@Component({
  selector: 'app-persona-moral-modal',
  templateUrl: './persona-moral-modal.component.html',
  styleUrls: ['./persona-moral-modal.component.scss']
})
export class PersonaMoralModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string) {}

  ngOnInit(): void {
  }

}
