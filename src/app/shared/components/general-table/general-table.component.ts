import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GeneralTable } from 'src/app/core/interface/general-table';
import {MatDialog} from '@angular/material/dialog';
import { PersonaMoralModalComponent } from '../modals/persona-moral-modal/persona-moral-modal.component';

@Component({
  selector: 'app-general-table',
  templateUrl: './general-table.component.html',
  styleUrls: ['./general-table.component.scss']
})
export class GeneralTableComponent implements OnInit {

  @Input() perfil: any;
  @Input() dataTable: GeneralTable[];

  @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

  rows: GeneralTable[] = [];
  temp = [];

  constructor(public dialog: MatDialog) {
    this.rows = this.dataTable;
    // this.temp = [...this.dataTable];
   }

  ngOnInit() {
    this.rows = this.dataTable;
    this.temp = [...this.dataTable];
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter((d) => {
      return d.asamblea.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  goToEdit(value: object) {
    // this.router.navigate(['albums/edit/' + value['id']]);
    console.log(value);
  }

  details(value: object) {
    const dialogRef = this.dialog.open(PersonaMoralModalComponent, {
      data: 'Emmtell'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
