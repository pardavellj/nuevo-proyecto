import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoPersonaFisicaComponent } from './contacto-persona-fisica.component';

describe('ContactoPersonaFisicaComponent', () => {
  let component: ContactoPersonaFisicaComponent;
  let fixture: ComponentFixture<ContactoPersonaFisicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactoPersonaFisicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoPersonaFisicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
