import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacto-persona-fisica',
  templateUrl: './contacto-persona-fisica.component.html',
  styleUrls: ['./contacto-persona-fisica.component.scss']
})
export class ContactoPersonaFisicaComponent implements OnInit {

  permisos = {campoUno: true, campoDos: false, campoTres: true, campoCuatro: false};
  datos: object;

  constructor() {
    this.datos = [
      { asamblea: 'datoAsamblea', secretario: 'datoSec', vocal: 'datoVocal', vocalDeportista: 'datoVocDep' },
      { asamblea: 'datoAsamblea2', secretario: 'datoSec2', vocal: 'datoVocal2', vocalDeportista: 'datoVocDep2' },
      { asamblea: 'datoAsamblea3', secretario: 'datoSec3', vocal: 'datoVocal3', vocalDeportista: 'datoVocDep3' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea4', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' },
      { asamblea: 'datoAsamblea5', secretario: 'datoSec4', vocal: 'datoVocal4', vocalDeportista: 'datoVocDep4' }
    ];
   }

  ngOnInit(): void {
  }

}
