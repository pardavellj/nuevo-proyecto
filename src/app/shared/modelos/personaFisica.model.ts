export class fisica {
    idPersona: number;
    paterno: string;
    materno: string;
    nombre: string;
    curp: string;
    fechaNacimiento: string;
    fechaRegistro: string;
    fechaModificacion: string;
    activo: number;
    idEscolaridad: number;
    idEstusEscolaridad: number;
    calle: string;
    numExt: number;
    NumInt: number;
    idGenero: number;
    extranjero: string;
    tipoIdentificacion: string;
    numeroIdentificacion: number;
    catUbicaciongeograficaId: number;
}