import { RouteInfo } from './sidebar.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [

    {
        path: '', title: 'Direcciones', icon: 'zmdi zmdi-home text-primary', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [

            { path: '/direcciones/badminton', title: 'Badminton', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            // { path: '/dashboard/m/v2', title: 'Dashboard 2', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            // { path: '/dashboard/m/v3', title: 'Dashboard 3', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            // { path: '/dashboard/m/v4', title: 'Dashboard 4', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        ]
    },
    {
        path: '', title: 'Federaciones', icon: 'zmdi zmdi-home text-primary', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
        submenu: [
            {
                path: '/federaciones/fisica/catalogo', title: 'Catalogo Federacion Fisica', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false,
                submenu: [
                    // { path: '/federaciones/fisica/catalogo', title: 'Catalogo', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
                ]
            },
            { path: '/federaciones/moral', title: 'Moral', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
            // { path: '/dashboard/m/v4', title: 'Dashboard 4', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        ]
    }
    // {
    //     path: '', title: 'Components', icon: 'zmdi zmdi-card-travel text-danger', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [

    //         { path: '/components/alerts', title: 'Alerts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/accordion', title: 'Accordion', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/bs-elements', title: 'BS Elements', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/buttons', title: 'Buttons', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/cards', title: 'Cards', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/collapse', title: 'Collapse', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/carousel', title: 'Carousel', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/dropdown', title: 'Dropdown', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/datepicker', title: 'Datepicker', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/list-groups', title: 'List Groups', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/modals', title: 'Modals', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/progress-bar', title: 'Progress Bars', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/pagination', title: 'Pagination', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/tabset', title: 'Tabset', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/timepicker', title: 'Timepicker', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/components/typography', title: 'Typography', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

    //     ]
    // },
    // {
    //     path: '', title: 'UI Elements', icon: 'zmdi zmdi-fire text-info', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [

    //         { path: '/ui-elements/toastr', title: 'Toastr', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/widgets', title: 'Widgets', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/switch', title: 'Switch', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/sweetalerts', title: 'Sweet Alert', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/tag-input', title: 'Tag Input', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/vertical-timeline', title: 'Vertical Timeline', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/horizontal-timeline', title: 'Horizontal Timeline', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/grid-layouts', title: 'Grid Layouts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/pricing-table', title: 'Pricing Table', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-elements/color-palette', title: 'Color Palette', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },

    //     ]
    // },
    // {
    //     path: '', title: 'Charts', icon: 'zmdi zmdi-chart text-success', class: 'sub', badge: '4', badgeClass: 'badge badge-info badge-pill float-right mr-1 mt-1', isExternalLink: false,
    //     submenu: [
    //         { path: '/charts/chartjs', title: 'ChartJs', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/charts/apex-charts', title: 'Apex Charts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/charts/sparkline-charts', title: 'Sparkline Charts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/charts/peity-charts', title: 'Peity Charts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/charts/other-charts', title: 'Other Charts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Authentication', icon: 'zmdi zmdi-lock text-warning', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/auth/signin', title: 'SignIn', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/auth/signup', title: 'SignUp', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/auth/lock-screen', title: 'Lock Screen', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/auth/reset-password', title: 'Reset Password', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //        ]
    // },
    // {
    //     path: '', title: 'Form', icon: 'zmdi zmdi-format-list-bulleted text-secondary', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/form/basic', title: 'Basic', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/basic-input', title: 'Basic Input', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/input-group', title: 'Input Group', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/form-layouts', title: 'Form Layouts', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/masks', title: 'Masks', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/editor', title: 'Editor', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/form/validation', title: 'Validation', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //        ]
    // },
    // { path: '/calendar', title: 'Calendar', icon: 'zmdi zmdi-calendar-check text-danger', class: '', badge: 'New', badgeClass: 'badge badge-success badge-pill float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
    // {
    //     path: '', title: 'Tables', icon: 'zmdi zmdi-grid', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/table/basic', title: 'Basic', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/table/responsive', title: 'Responsive', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //        ]
    // },
    // {
    //     path: '', title: 'Data Tables', icon: 'fa fa-database text-success', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/datatable/basic', title: 'Basic', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/fullscreen', title: 'Fullscreen', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/editing', title: 'Editing', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/filter', title: 'Filter', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/paging', title: 'Paging', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/pinning', title: 'Pinning', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/selection', title: 'Selection', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/datatable/sorting', title: 'Sorting', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //       ]
    // },
    // {
    //     path: '', title: 'UI Icons', icon: 'zmdi zmdi-invert-colors text-primary', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/ui-icons/font-awesome-icon', title: 'Font Awesome icon', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-icons/material-design', title: 'Material Design', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-icons/themify', title: 'Themify', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/ui-icons/line-icons', title: 'Line Icons', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Maps', icon: 'zmdi zmdi-map text-youtube', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/maps/google', title: 'Google Maps', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/maps/fullscreen', title: 'Full Screen Map', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Pages', icon: 'zmdi zmdi-collection-folder-image text-dribbble', class: 'sub', badge: '', badgeClass: '', isExternalLink: false,
    //     submenu: [
    //         { path: '/pages/profile', title: 'Profile', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/invoice', title: 'Invoice', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/blank-page', title: 'Blank Page', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/coming-soon', title: 'Coming Soon', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/error-403', title: 'Error 403', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/error-404', title: 'Error 404', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //         { path: '/pages/error-500', title: 'Error 500', icon: 'fa fa-dot-circle-o', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    //     ]
    // },
    // { path: 'http://codervent.com/wipe-admin/docs/', title: 'Documentation', icon: 'fa fa-address-book text-info', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] },
    // { path: 'https://themeforest.net/user/codervent/portfolio', title: 'Support', icon: 'zmdi zmdi-help-outline text-warning', class: '', badge: '', badgeClass: '', isExternalLink: true, submenu: [] }


];
