import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarMatComponent } from './sidebar-mat.component';

describe('SidebarMatComponent', () => {
  let component: SidebarMatComponent;
  let fixture: ComponentFixture<SidebarMatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarMatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarMatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
